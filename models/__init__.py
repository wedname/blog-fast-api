import importlib
import os

# Get a list of files in the model folder
model_files = os.listdir('models')

# Import all modules from models folder
for file in model_files:
    if file.endswith('.py') and file != '__init__.py':
        module_name = file[:-3]  # Remove .py extension
        module = importlib.import_module(f'models.{module_name}')
        globals().update(vars(module))
