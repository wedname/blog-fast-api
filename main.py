from fastapi import FastAPI
from config.database import engine, Base

import models

app = FastAPI()

Base.metadata.create_all(bind=engine)
