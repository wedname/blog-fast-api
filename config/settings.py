import os

from dotenv import load_dotenv
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    test_db_url: str = os.environ["TEST_DB_URL"]
    main_db_url: str = os.environ["MAIN_DB_URL"]


settings = Settings()
